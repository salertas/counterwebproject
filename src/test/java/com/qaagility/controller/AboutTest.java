package com.qaagility.controller;

import org.junit.Assert;
import org.junit.Test;

public class AboutTest {

	@Test
	public void testDesc() throws Exception {
		String result = new About().desc();
		String expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
		Assert.assertEquals(result, expected);
	}

}
